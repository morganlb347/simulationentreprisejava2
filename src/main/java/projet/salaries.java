package projet;

class Salaries {

    private String nom;
    private int salaire;
    private boolean fonction;

    public salaries(int salaire,String nom,boolean fonction) {
        this.salaire = salaire;
        this.nom=nom;
        this.fonction=fonction;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getSalaire() {
        return salaire;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }

    public boolean isFonction() {
        return fonction;
    }

    public void setFonction(boolean fonction) {
        this.fonction = fonction;
    }


}
