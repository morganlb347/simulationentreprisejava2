package projet;

class simulation {
    private Entreprise uneentreprise;
    private int nbmois;

     public simulation(Entreprise uneentreprise, int nbmois) {
         this.uneentreprise = uneentreprise;
         this.nbmois=nbmois;
     }

     public Entreprise getUneentreprise() {
         return uneentreprise;
     }

     public void setUneentreprise(Entreprise uneentreprise) {
         this.uneentreprise = uneentreprise;
     }

     public int getNbmois() {
         return nbmois;
     }

     public void setNbmois(int nbmois) {
         this.nbmois = nbmois;
     }


 }
