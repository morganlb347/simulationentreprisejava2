package projet;

class Voiture {
private Entreprise entreprise;
private int prix;
private int coutentretien;

    public Voiture(int prix, int coutentretien, Entreprise uneentreprise){
        this.prix=prix;
        this.coutentretien=coutentretien;
        this.entreprise=uneentreprise;}

    public int getPrix() {
        return prix;
    }

    public int getCoutentretien() {
        return coutentretien;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setCoutentretien(int coutentretien) {
        this.coutentretien = coutentretien;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }


}
