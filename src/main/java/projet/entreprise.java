package projet;

import java.util.ArrayList;

class Entreprise{
    private int ca;
    private String nomentreprise;
    private ArrayList liste_salaries,liste_Bureaux,liste_Voiture;
    public Entreprise(int ca,String nomentreprise){
        this.ca=ca;
        this.nomentreprise=nomentreprise;

        this.liste_salaries=new ArrayList();
        this.liste_Bureaux=new ArrayList();
        this.liste_salaries=new ArrayList();

    }

    public ArrayList getListe_salaries() {
        return liste_salaries;
    }

    public void setListe_salaries(ArrayList liste_salaries) {
        this.liste_salaries = liste_salaries;
    }


    public ArrayList getListe_Bureaux() {
        return liste_Bureaux;
    }

    public void setListe_Bureaux(ArrayList liste_Bureaux) {
        this.liste_Bureaux = liste_Bureaux;
    }

    public ArrayList getListe_Voiture() {
        return liste_Voiture;
    }

    public void setListe_Voiture(ArrayList liste_Voiture) {
        this.liste_Voiture = liste_Voiture;
    }


    public int getCa() {
        return ca;
    }

    public void setCa(int ca) {
        this.ca = ca;
    }

    public String getNomentreprise() {
        return nomentreprise;
    }

    public void setNomentreprise(String nomentreprise) {
        this.nomentreprise = nomentreprise;
    }


}