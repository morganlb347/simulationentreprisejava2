package projet;

class bureaux {
     private String adresse;
     private int prix;

    public bureaux(String adresse,int prix) {
        this.adresse=adresse;
        this.prix=prix;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }


}
